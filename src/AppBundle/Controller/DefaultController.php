<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("user/create",name="userCreate")
     */
    public function createUser(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $name = $request->get('name',NULL);
        $email = $request->get('email',NULL);
        $phone = $request->get('phone',NULL);
        $address = $request->get('address',null);
        if($phone && $email  && $name){


            $object = new User();
            $object->setEmail($email);
            $object->setAddress($address);
            $object->setName($name);
            $object->setPhone($phone);
            $em->persist($object);
            $em->flush();
            $arr = [
                'status' => 200,
                'msg' => 'User Created Successfully'
            ];
            return new Response(json_encode($arr));

        }else{
            $arr = [
                'status' => 500,
                'msg' => 'Required fields are empty'
            ];

            return new  Response(json_encode($arr));
        }
    }


    /**
     * @param Request $request
     * @Route("user/{userid}",name="d=fetchUser")
     */
    public function user(Request $request,$userid = null)
    {
        if($userid){
       $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($userid);
       if($user){

           $arr = [
               'id' => $user->getId(),
               'name' => $user->getName(),
               'address' => $user->getAddress(),
               'phone' => $user->getPhone()
           ];
           return new  Response(json_encode($arr));
       }else{
           $arr = [
               'status' => 500,
               'msg' => 'Requested User not found'
           ];
           return new  Response(json_encode($arr));
       }
        }else{
            $arr = [
                'status' => 500,
                'msg' => 'user id is required'
            ];

            return new  Response(json_encode($arr));
        }



    }

}
